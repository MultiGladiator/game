package com.gitlab.laughingdinosaur.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class MyGdxGame extends ApplicationAdapter {

  @Override
  public void create() {
    log.error("TEST");
  }

  @Override
  public void render() {
    Gdx.gl.glClearColor(1, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
  }

  @Override
  public void dispose() {}
}
